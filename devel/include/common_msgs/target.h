// Generated by gencpp from file common_msgs/target.msg
// DO NOT EDIT!


#ifndef COMMON_MSGS_MESSAGE_TARGET_H
#define COMMON_MSGS_MESSAGE_TARGET_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>
#include <geometry_msgs/Point.h>

namespace common_msgs
{
template <class ContainerAllocator>
struct target_
{
  typedef target_<ContainerAllocator> Type;

  target_()
    : header()
    , tgt()
    , yaw_tgt(0.0)
    , engaged(false)
    , controller(false)  {
    }
  target_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , tgt(_alloc)
    , yaw_tgt(0.0)
    , engaged(false)
    , controller(false)  {
  (void)_alloc;
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef  ::geometry_msgs::Point_<ContainerAllocator>  _tgt_type;
  _tgt_type tgt;

   typedef double _yaw_tgt_type;
  _yaw_tgt_type yaw_tgt;

   typedef uint8_t _engaged_type;
  _engaged_type engaged;

   typedef uint8_t _controller_type;
  _controller_type controller;





  typedef boost::shared_ptr< ::common_msgs::target_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::common_msgs::target_<ContainerAllocator> const> ConstPtr;

}; // struct target_

typedef ::common_msgs::target_<std::allocator<void> > target;

typedef boost::shared_ptr< ::common_msgs::target > targetPtr;
typedef boost::shared_ptr< ::common_msgs::target const> targetConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::common_msgs::target_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::common_msgs::target_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace common_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'common_msgs': ['/home/jy/HK_quad/src/simlite-rpt-master/common_msgs/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::common_msgs::target_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::common_msgs::target_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::common_msgs::target_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::common_msgs::target_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::common_msgs::target_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::common_msgs::target_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::common_msgs::target_<ContainerAllocator> >
{
  static const char* value()
  {
    return "043d491352f610db77be4c64a7744a69";
  }

  static const char* value(const ::common_msgs::target_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x043d491352f610dbULL;
  static const uint64_t static_value2 = 0x77be4c64a7744a69ULL;
};

template<class ContainerAllocator>
struct DataType< ::common_msgs::target_<ContainerAllocator> >
{
  static const char* value()
  {
    return "common_msgs/target";
  }

  static const char* value(const ::common_msgs::target_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::common_msgs::target_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Header header\n\
geometry_msgs/Point tgt\n\
float64 yaw_tgt\n\
bool engaged\n\
bool controller  # 0 for pos; 1 for vel\n\
\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Point\n\
# This contains the position of a point in free space\n\
float64 x\n\
float64 y\n\
float64 z\n\
";
  }

  static const char* value(const ::common_msgs::target_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::common_msgs::target_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.tgt);
      stream.next(m.yaw_tgt);
      stream.next(m.engaged);
      stream.next(m.controller);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct target_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::common_msgs::target_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::common_msgs::target_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "tgt: ";
    s << std::endl;
    Printer< ::geometry_msgs::Point_<ContainerAllocator> >::stream(s, indent + "  ", v.tgt);
    s << indent << "yaw_tgt: ";
    Printer<double>::stream(s, indent + "  ", v.yaw_tgt);
    s << indent << "engaged: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.engaged);
    s << indent << "controller: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.controller);
  }
};

} // namespace message_operations
} // namespace ros

#endif // COMMON_MSGS_MESSAGE_TARGET_H
