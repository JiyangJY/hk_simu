// Auto-generated. Do not edit!

// (in-package common_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let geometry_msgs = _finder('geometry_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class target {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.tgt = null;
      this.yaw_tgt = null;
      this.engaged = null;
      this.controller = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('tgt')) {
        this.tgt = initObj.tgt
      }
      else {
        this.tgt = new geometry_msgs.msg.Point();
      }
      if (initObj.hasOwnProperty('yaw_tgt')) {
        this.yaw_tgt = initObj.yaw_tgt
      }
      else {
        this.yaw_tgt = 0.0;
      }
      if (initObj.hasOwnProperty('engaged')) {
        this.engaged = initObj.engaged
      }
      else {
        this.engaged = false;
      }
      if (initObj.hasOwnProperty('controller')) {
        this.controller = initObj.controller
      }
      else {
        this.controller = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type target
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [tgt]
    bufferOffset = geometry_msgs.msg.Point.serialize(obj.tgt, buffer, bufferOffset);
    // Serialize message field [yaw_tgt]
    bufferOffset = _serializer.float64(obj.yaw_tgt, buffer, bufferOffset);
    // Serialize message field [engaged]
    bufferOffset = _serializer.bool(obj.engaged, buffer, bufferOffset);
    // Serialize message field [controller]
    bufferOffset = _serializer.bool(obj.controller, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type target
    let len;
    let data = new target(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [tgt]
    data.tgt = geometry_msgs.msg.Point.deserialize(buffer, bufferOffset);
    // Deserialize message field [yaw_tgt]
    data.yaw_tgt = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [engaged]
    data.engaged = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [controller]
    data.controller = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 34;
  }

  static datatype() {
    // Returns string type for a message object
    return 'common_msgs/target';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '043d491352f610db77be4c64a7744a69';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    geometry_msgs/Point tgt
    float64 yaw_tgt
    bool engaged
    bool controller  # 0 for pos; 1 for vel
    
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new target(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.tgt !== undefined) {
      resolved.tgt = geometry_msgs.msg.Point.Resolve(msg.tgt)
    }
    else {
      resolved.tgt = new geometry_msgs.msg.Point()
    }

    if (msg.yaw_tgt !== undefined) {
      resolved.yaw_tgt = msg.yaw_tgt;
    }
    else {
      resolved.yaw_tgt = 0.0
    }

    if (msg.engaged !== undefined) {
      resolved.engaged = msg.engaged;
    }
    else {
      resolved.engaged = false
    }

    if (msg.controller !== undefined) {
      resolved.controller = msg.controller;
    }
    else {
      resolved.controller = false
    }

    return resolved;
    }
};

module.exports = target;
