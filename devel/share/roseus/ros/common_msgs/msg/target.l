;; Auto-generated. Do not edit!


(when (boundp 'common_msgs::target)
  (if (not (find-package "COMMON_MSGS"))
    (make-package "COMMON_MSGS"))
  (shadow 'target (find-package "COMMON_MSGS")))
(unless (find-package "COMMON_MSGS::TARGET")
  (make-package "COMMON_MSGS::TARGET"))

(in-package "ROS")
;;//! \htmlinclude target.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass common_msgs::target
  :super ros::object
  :slots (_header _tgt _yaw_tgt _engaged _controller ))

(defmethod common_msgs::target
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:tgt __tgt) (instance geometry_msgs::Point :init))
    ((:yaw_tgt __yaw_tgt) 0.0)
    ((:engaged __engaged) nil)
    ((:controller __controller) nil)
    )
   (send-super :init)
   (setq _header __header)
   (setq _tgt __tgt)
   (setq _yaw_tgt (float __yaw_tgt))
   (setq _engaged __engaged)
   (setq _controller __controller)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:tgt
   (&rest __tgt)
   (if (keywordp (car __tgt))
       (send* _tgt __tgt)
     (progn
       (if __tgt (setq _tgt (car __tgt)))
       _tgt)))
  (:yaw_tgt
   (&optional __yaw_tgt)
   (if __yaw_tgt (setq _yaw_tgt __yaw_tgt)) _yaw_tgt)
  (:engaged
   (&optional __engaged)
   (if __engaged (setq _engaged __engaged)) _engaged)
  (:controller
   (&optional __controller)
   (if __controller (setq _controller __controller)) _controller)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Point _tgt
    (send _tgt :serialization-length)
    ;; float64 _yaw_tgt
    8
    ;; bool _engaged
    1
    ;; bool _controller
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Point _tgt
       (send _tgt :serialize s)
     ;; float64 _yaw_tgt
       (sys::poke _yaw_tgt (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; bool _engaged
       (if _engaged (write-byte -1 s) (write-byte 0 s))
     ;; bool _controller
       (if _controller (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Point _tgt
     (send _tgt :deserialize buf ptr-) (incf ptr- (send _tgt :serialization-length))
   ;; float64 _yaw_tgt
     (setq _yaw_tgt (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; bool _engaged
     (setq _engaged (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _controller
     (setq _controller (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get common_msgs::target :md5sum-) "043d491352f610db77be4c64a7744a69")
(setf (get common_msgs::target :datatype-) "common_msgs/target")
(setf (get common_msgs::target :definition-)
      "Header header
geometry_msgs/Point tgt
float64 yaw_tgt
bool engaged
bool controller  # 0 for pos; 1 for vel


================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

")



(provide :common_msgs/target "043d491352f610db77be4c64a7744a69")


