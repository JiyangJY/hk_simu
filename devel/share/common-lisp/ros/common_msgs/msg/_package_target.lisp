(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          TGT-VAL
          TGT
          YAW_TGT-VAL
          YAW_TGT
          ENGAGED-VAL
          ENGAGED
          CONTROLLER-VAL
          CONTROLLER
))