(cl:in-package common_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          POS-VAL
          POS
          VEL-VAL
          VEL
          ACC-VAL
          ACC
          YAW-VAL
          YAW
))