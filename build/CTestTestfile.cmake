# CMake generated Testfile for 
# Source directory: /home/jy/HK_quad/src
# Build directory: /home/jy/HK_quad/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(hector_models/hector_components_description)
subdirs(simlite-rpt-master/hector_gazebo/hector_gazebo)
subdirs(simlite-rpt-master/hector_gazebo/hector_gazebo_worlds)
subdirs(hector_models/hector_models)
subdirs(simlite-rpt-master/hector_quadrotor/hector_quadrotor)
subdirs(simlite-rpt-master/hector_quadrotor/hector_quadrotor_demo)
subdirs(simlite-rpt-master/hector_quadrotor/hector_quadrotor_description)
subdirs(hector_models/hector_sensors_description)
subdirs(simlite-rpt-master/hector_gazebo/hector_sensors_gazebo)
subdirs(hector_models/hector_xacro_tools)
subdirs(simlite-rpt-master/hector_quadrotor/hector_uav_msgs)
subdirs(simlite-rpt-master/hector_quadrotor/hector_quadrotor_model)
subdirs(mobile_cylinder)
subdirs(simlite-rpt-master/hector_quadrotor/hector_quadrotor_teleop)
subdirs(simlite-rpt-master/common_msgs)
subdirs(simlite-rpt-master/hector_gazebo/hector_gazebo_plugins)
subdirs(simlite-rpt-master/hector_quadrotor/hector_quadrotor_controller)
subdirs(simlite-rpt-master/hector_quadrotor/hector_quadrotor_gazebo_plugins)
subdirs(simlite-rpt-master/hector_quadrotor_reference)
subdirs(simlite-rpt-master/hector_quadrotor/hector_quadrotor_controller_gazebo)
subdirs(simlite-rpt-master/hector_quadrotor/hector_quadrotor_gazebo)
